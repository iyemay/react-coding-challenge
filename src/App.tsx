import React from 'react';
import './App.css';
import StartComponent from './components/StartComponent';
import LastComponent from './components/LastComponent';
import QuestionsComponent from "./components/QuestionsComponent";

function App() {
  return (
    <div>
      <QuestionsComponent />
    </div>
  );
}

export default App;
