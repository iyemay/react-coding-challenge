import React, { Component, MouseEvent } from "react";
import QuestionsComponent from "./QuestionsComponent";

interface IState {
    showComponent: boolean;
}

export default class StartComponent extends Component<{}, IState> {
    constructor(props: any) {
        super(props);
        this.state = {
            showComponent: false,
        }
        this.handleClick = this.handleClick.bind(this);
    }


    handleClick(event: MouseEvent<HTMLButtonElement>) {
        this.setState({
            showComponent: true,
        });
    }

    render () {
        return (
            <div className="start-container">
                <i className="fa fa-university icon-start" />
                <p className="size-title">
                    How are you doing with your money? Take a 2-minute quiz.
                </p>
                <button className="start-button" onClick={this.handleClick}>Let's Do This</button>
                {this.state.showComponent ? <QuestionsComponent /> : null }
            </div>
        );
    }
}

